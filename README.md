# SimpleSAMLphp LogAnalysis

This tool operates on STAT logs from a SimpleSAMLphp IdP (both compressed and uncompressed) and is used to collect usage statistics.

Example log lines parsed:

```bash
Feb  7 12:32:12 ssp-idp simplesamlphp[27612]: 5 STAT [46ff6971c4] saml20-idp-SSO https://sp.aai-test.garr.it/shibboleth https://ssp-idp.aai-test.garr.it/simplesaml-212/module.php/saml/idp/metadata NA
Feb  7 12:32:12 ssp-idp simplesamlphp[27612]: 5 STAT [46ff6971c4] consent nostorage
Feb  7 12:32:19 ssp-idp simplesamlphp[27636]: 5 STAT [46ff6971c4] consentResponse rememberNot
Feb  9 10:27:19 ssp-idp simplesamlphp[62995]: 5 STAT [2d3387ea6f] User 'admin' successfully authenticated from 90.147.163.3
```

## Requirements

* PHP >= 5

## Installation

This tool has no external dependencies other than a standard PHP runtime.
Just make the script executable and copy it into your `$PATH`. E.g.:

```sh
$ chmod +x ssp-loganalysis.php && sudo cp ssp-loganalysis.php /usr/local/bin/
```

In case your OS only provides a `php` callable:

```sh
ssp-loganalysis.php /var/log/simplesamlphp.stat

ssp-loganalysis.php /var/log/simplesamlphp-stat.tar.gz
```

Example JSON Output:

```json
{
    "stats": {
        "logins": 17,
        "rps": 1,
        "ssp-version": "1.19.8"
    },
    "logins_per_rp": {
        "https://sp.aai-test.garr.it/shibboleth": 17
    }
} 
```

## Author

Marco Malavolti
